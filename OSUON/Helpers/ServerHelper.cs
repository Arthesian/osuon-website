﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Hosting;

namespace OSUON.Helpers
{
    public static class ServerHelper
    {
        public static string RelativePath(string path)
        {
            return path.Replace(HostingEnvironment.ApplicationPhysicalPath, "~/").Replace(@"\", "/");
        }
    }
}
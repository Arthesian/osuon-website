var OSUON = OSUON || {};
OSUON.game = OSUON.game || {};

/**
 * Controls module for OSUON.
 * The bottom 'strip' with buttons like 'mute' and 'fullscreen'
 * are defined here
 */
OSUON.game.controls = (function(){
    var _ = {};

    _.variables = {
        gameContainerSelector: ".game__container",
        isFullScreen : false,
        isMuted : false
    };

    /**
     * Set the game to fullscreen mode;
     * 100% width and heigt of the window, while respecting aspect ratio
     */
    _.setFullScreen = function(enable){
        var gameContainer = document.querySelector(_.variables.gameContainerSelector);
        
        var button = document.querySelector(".game__controls__fullscreen span")

        if(enable){
            document.body.style.overflow = "hidden";
            button.classList.remove("fa-expand");
            button.classList.add("fa-compress");
            gameContainer.classList.add("fullscreen");
            gameContainer.style.zIndex = 2000;
        }else{
            gameContainer.classList.remove("fullscreen");
            button.classList.remove("fa-compress");
            button.classList.add("fa-expand");
            document.body.style.overflow = "";
            gameContainer.style.zIndex = "auto";
        }
        _.variables.isFullScreen = enable;
    }

    /**
     * Toggle fullscreen
     */
    _.toggleFullScreen = function(){
        _.setFullScreen(!_.variables.isFullScreen);
    }

    /**
     * Mute the sound of the game
     */
    _.setMute = function(enable){
        // Mute the actual Game Engine,, depends on the actual game project
        // Something like :
        // Game.Sound.Mute(enable);

        console.warn("Not yet implemented!");

        _.variables.isMuted = enable;
    }

    /**
     * Toggle mute
     */
    _.toggleMute = function(){
        _.setMute(!_.variables.isMuted);
    }

    return _;
})();
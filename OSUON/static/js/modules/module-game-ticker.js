﻿var OSUON = OSUON || {};
OSUON.game = OSUON.game || {};

/**
 * Ticker that ticks ever 'frame'. The engine so to speak of the renderer.
 * Defaults to 30 FPS, maximum is 250 FPS ( browser limit for setInterval )
 */
OSUON.game.ticker = function (fps) {

    /**
     * Scope 
     */
    var _ = {}

    /**
     * Private: 
     * Variabled used by this module
     */
    var variables = {
        targetFps: fps,
        frameCount: [],
        interval: null,
        isPauzed: false
    }

    /**
     * Settings for the Ticker
     */
    _.settings = {
        onTick: function () { }
    }

    /**
     * Start the Ticker
     */
    _.start = function () {
        variables.isPauzed = false;

        variables.interval = setInterval(function () {
            _.tick();
        }, 1000 / (variables.targetFps || 30));
    }

    /**
     * Stop the Ticker from updating 
     */
    _.stop = function () {
        variables.isPauzed = true;
        clearInterval(variables.interval);
    }

    /**
     * Tick method which is called every 'frame'
     */
    _.tick = function () {
        updateFpsFrameCount();
        _.settings.onTick();
    }

    /**
     * Private: 
     * Keeps track of the amount of 'ticks' that where
     * made in the last 1000ms ( 1 second )
     */
    var updateFpsFrameCount = function () {
        variables.frameCount.push(new Date());
        variables.frameCount = variables.frameCount.filter(function (d) {
            return d.getTime() > (new Date().getTime() - 1000)
        });
    }

    /**
     * Get the number of 'ticks' in the previous second
     */
    _.getMeasuredFps = function () {
        return variables.frameCount.length;
    }

    /**
     * Return the scope object
     */
    return _;

};
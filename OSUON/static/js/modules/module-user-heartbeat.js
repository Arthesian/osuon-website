var OSUON = OSUON || {};
OSUON.user = OSUON.user || {};

/**
 * Heartbeat module for the user that is related to 
 * the user's online activity. General rule :
 * - Every 'Alive()' call, keeps the user 'Online' for X minutes
 * - Every 'HeartBeat()' interval call, will keep the user 'inactve'
 *      when the 'Alive()' call hasn't been made after some time
 * - If no 'HeartBeat()' is sent after X seconds, the user is considered offline
 */
OSUON.user.heartbeat = (function ($) {

    var _ = {};

    /**
     * Heartbeat pings server on each page load or when it's called
     * by page code. This signifies that the user is actively using
     * the site ( instead of the heartbeatInterval )
     */
    _.alive = function () {
        $.ajax({
            url: "/user/heartbeat",
            type: "POST",
            data: {
                real : true
            }
        });
    }

    /**
     * HeartbeatInterval pings server every couple of seconds
     * to update the user's last-active timestamp. Is 
     * used to record the user's status.
     */
    var heartbeatInterval = (function () {

        setInterval(function () {
            $.ajax("/user/heartbeat");
        }, 2000);

    })();

    _.getUserStatus = function (id) {

        var statusses = [];

        if (id) {

            $.post("/user/getuserstatus", { id: id }).done(function (data) {
                statusses.push(data);

                statusses.forEach(function (status) {
                    var node = document.querySelectorAll("[data-user=" + status.userName + "].status");

                    if (node) {
                        node.forEach(function (n) {
                            n.setAttribute("data-user-status", status.status);
                        });
                    }
                });
            });

        } else {
            console.warn("No user ID was supplied to 'getUserStatus(id)'!"); return;
        }

    };

    var updateUsersStatus = (function () {

        setInterval(function () {

            $.post("/users/getusersstatus").done(function (data) {

                data.forEach(function (status) {
                    var node = document.querySelectorAll("[data-user=" + status.userName + "].status");

                    if (node) {
                        node.forEach(function (n) {
                            n.setAttribute("data-user-status", status.status);
                        });
                    }
                });
            });

        }, 5000);

    })();

    return _;

})(jQuery);

// Call the heartbeat on page load
OSUON.user.heartbeat.alive();
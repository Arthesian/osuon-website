﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OSUON.Managers
{
    public class SessionManager
    {
        public T Get<T>(string key)
        {
            var result = (T)HttpContext.Current.Session[key];

            return result;
        }

        public void Set(object value, string key)
        {
            HttpContext.Current.Session.Add(key, value);
        }

        public void Delete(string key)
        {
            HttpContext.Current.Session.Remove(key);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OSUON.Models
{
    public class GameSettingsModel
    {
        public SkinModel SelectedSkin { get; set; }
        public float BackgroundAlpha { get; set; }
    }
}
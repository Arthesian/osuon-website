﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OSUON.Models
{
    public class RegisterModel
    {
        [Required]
        [MinLength(3)]
        public string Username { get; set; }

        [Required]
        [MinLength(8)]
        public string Password { get; set; }

        [Compare("Password")]
        [Required]
        public string RepeatPassword { get; set;}

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required(ErrorMessage = "You need to accept the terms before you can make use of this platform")]
        public bool AcceptTerms { get; set; }
    }
}
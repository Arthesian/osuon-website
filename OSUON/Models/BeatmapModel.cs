﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OSUON.Models
{
    public class BeatmapModel
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        [BsonIgnoreIfDefault]
        public string Id { get; set; }
        [BsonRepresentation(BsonType.ObjectId)]
        public string OwnerId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Artist { get; set; }
        public string Source { get; set; }

        public List<BeatmapDifficultyModel> Difficulties { get; set; }
    }
}
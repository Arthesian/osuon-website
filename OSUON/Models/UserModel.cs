﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace OSUON.Models
{
    [BsonIgnoreExtraElements]
    public class UserModel
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        [BsonIgnoreIfDefault]
        public string Id { get; set; }
        public string Username { get; set; }
        public string PasswordHash { get; set; }
        public string Email { get; set; }
        public string ProfileImageUrl { get; set; }
        public string CultureISOCode { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public DateTime RegisterDate { get; set; }
        public DateTime BirthDay { get; set; }
        public List<string> Hobbies { get; set; }
        public string Occupation { get; set; }
        public string WebsiteUrl { get; set; }
        public string About { get; set; }

        public DateTime LastActivity { get; set; }
        public DateTime LastHeartBeat { get; set; }

        [BsonIgnore]
        public CultureInfo Culture {
            get
            {
                if (string.IsNullOrEmpty(CultureISOCode))
                {
                    CultureISOCode = "en-US";
                }
                return new CultureInfo(CultureISOCode);
            }
            set
            {
                CultureISOCode = Culture.Name;
            }
        }
        [BsonIgnore]
        public string Status { get
            {
                if (LastHeartBeat.AddSeconds(10) < DateTime.UtcNow) { return "OFFLINE"; }
                if (LastActivity.AddMinutes(1) > DateTime.UtcNow) { return "ONLINE"; }
                else return "INACTIVE";
            }
        }

        public UserModel() { }
    }
}
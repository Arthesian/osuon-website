﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OSUON.Models
{
    public class BeatmapDifficultyModel
    {
        public string Name { get; set; }
        public float Stars { get; set; }
        public string Data { get; set; }
        public float Rating { get; }
        public int Votes { get; }
        public int PlayCount { get; }
        public int TotalPlayCount { get; }
    }
}
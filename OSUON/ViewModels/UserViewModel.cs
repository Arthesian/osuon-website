﻿using OSUON.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OSUON.ViewModels
{
    public class UserViewModel
    {
        public UserModel User { get; set; }

        public Dictionary<string, string> AboutList { get; set; }

        public Dictionary<string, string> Awards { get; set; }

        public Dictionary<string, string> Statistics { get; set; }

        public List<BeatmapModel> RecentPlays { get; set; }
        
        // Top Scores?

        // Achievements?

        // Settings?

        // Friends?
        public List<UserModel> Friends { get; set; }
    }
}
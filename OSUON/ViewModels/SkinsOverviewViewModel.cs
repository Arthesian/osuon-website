﻿using OSUON.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OSUON.ViewModels
{
    public class SkinsOverviewViewModel : BaseViewModel
    {
        public List<SkinModel> Skins { get; set; }

        // Statistics
        
        // Image previews?

        // Download link

        // etc.
    }
}
﻿using OSUON.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OSUON.ViewModels
{
    public class UsersOverviewViewModel : BaseViewModel
    {
        public List<UserModel> ActiveUsers { get; set; }
        public List<UserModel> Users { get; set; }
    }
}
﻿using OSUON.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OSUON.ViewModels
{
    public class BaseViewModel
    {
        public UserModel CurrentUser { get { return (UserModel)HttpContext.Current.Session["CurrentUser"]; } }

        public BaseViewModel()
        {

        }
    }
}
﻿using OSUON.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OSUON.ViewModels
{
    public class GameViewModel : BaseViewModel
    {
        public List<SkinModel> Skins { get; set; }
        public List<BeatmapModel> Beatmaps { get; set; }
        public GameSettingsModel Settings { get; set; }
    }
}
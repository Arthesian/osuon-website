﻿using OSUON.Models;
using OSUON.Repositories;
using OSUON.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using OSUON.Helpers;
using System.Text;

namespace OSUON.Controllers
{
    public class BeatmapsController : BaseController
    {
        public BeatmapRepository Repository { get; set; }

        public BeatmapsController()
        {
            Repository = new BeatmapRepository();
        }

        // GET: Beatmap
        public ActionResult Index()
        {
            var model = new BeatmapsOverviewViewModel();

            model.Beatmaps = new List<BeatmapModel>();

            for(var x = 0; x < 22; x++)
            {
                model.Beatmaps.Add(Repository.GetDummyBeatmap());
            }

            return View(model);
        }

        [HttpGet]
        [OutputCache(Duration = 3600)]
        public ActionResult GetJson()
        {
            var beatmaps = new List<BeatmapApiObject>();

            var folders = new DirectoryInfo(HostingEnvironment.MapPath("~/Assets/Beatmaps/"));

            foreach(var folder in folders.GetDirectories())
            {
                var beatmap = new BeatmapApiObject();
                beatmap.dir = ServerHelper.RelativePath(folder.FullName);

                var regex = new Regex(@"(?<id>\d+) (?<name>(?<artist>.+) - (?<song>.+))");
                var match = regex.Match(folder.Name);

                beatmap.id = match.Groups["id"].Value;
                beatmap.artist = match.Groups["artist"].Value;
                beatmap.song = match.Groups["song"].Value;
                beatmap.name = match.Groups["name"].Value;

                string songFile = string.Empty;

                foreach(var file in folder.GetFiles())
                {
                    beatmap.contents.Add(file.Name);

                    if(file.Extension == ".mp3")
                    {
                        songFile = ServerHelper.RelativePath(file.FullName);
                    }

                    if(file.Extension == ".osu")
                    {
                        var beatmapDifficulty = new Beatmap_DifficultyApiObject();

                        beatmapDifficulty.dir = ServerHelper.RelativePath(file.FullName);
                        beatmapDifficulty.difficulty = file.Name.Split('[')[1].Split(']')[0];

                        beatmap.beatmaps.Add(beatmapDifficulty);
                    }
                }

                beatmap.beatmaps.ForEach(x => { x.song = songFile; });

                beatmaps.Add(beatmap);
            }

            return Json(beatmaps, "application/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }
        public class BeatmapApiObject
        {
            public string dir { get; set; }
            public string id { get; set; }
            public string name { get; set; }
            public string artist { get; set; }
            public string song { get; set; }
            public List<string> contents { get; set; }
            public List<Beatmap_DifficultyApiObject> beatmaps { get; set; }

            public BeatmapApiObject()
            {
                this.contents = new List<string>();
                this.beatmaps = new List<Beatmap_DifficultyApiObject>();
            }
        }

        public class Beatmap_DifficultyApiObject
        {
            public string difficulty { get; set; }
            public string dir { get; set; }
            public string song { get; set; }
        }
    }
}
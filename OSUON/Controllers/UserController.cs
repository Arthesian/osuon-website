﻿using OSUON.Models;
using OSUON.Repositories;
using OSUON.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace OSUON.Controllers
{
    public class UserController : BaseController
    {
        public UserRepository Repository { get; set; }

        public UserController()
        {
            Repository = new UserRepository();
        }

        public ActionResult Index()
        {
            return RedirectToAction("Profile");
        }

        public ActionResult Settings()
        {
            if (CurrentUser == null)
            {
                return View();
            }

            return View(CurrentUser);
        }

        public ActionResult Edit()
        {
            if(CurrentUser == null)
            {
                return View();
            }

            return View(CurrentUser);
        }

        [HttpPost]
        public ActionResult Edit(UserModel model)
        {
            var user = Repository.ProfileUpdate(CurrentUser, model);

            Repository.UpdateUser(user);

            // If all went well, redirect to profile page
            return RedirectToAction("Profile/Id/" + user.Id);
        }

        public new ActionResult Profile()
        {
            if(CurrentUser == null)
            {
                return View();
            }

            return ProfileById(CurrentUser.Id);
        }

        [Route("User/Profile/Id/{id}")]
        public ActionResult ProfileById(string id)
        {
            var model = new UserViewModel();

            var user = Repository.GetUserById(id);

            if(user == null)
            {
                return View();
            }

            model = Repository.GetViewModel(user);

            return View("Profile", model);
        }

        [Route("User/Profile/{username}")]
        public new ActionResult Profile(string username)
        {
            if(string.IsNullOrEmpty(username) && CurrentUser == null)
            {
                return View();
            }
            else if(string.IsNullOrEmpty(username) && CurrentUser != null)
            {
                return ProfileById(CurrentUser.Id);
            }
            else
            {
                var user = Repository.GetUserByUsername(username);

                if(user != null)
                {
                    return ProfileById(user.Id);
                }

                return View();
            }
        }

        public ActionResult Register()
        {
            var model = new RegisterModel();

            return View(model);
        }

        [HttpPost]
        public ActionResult Register(RegisterModel model)
        {
            if (!model.AcceptTerms)
            {
                ModelState.AddModelError("AcceptTerms", "You need to accept the terms to use this platform");
            }

            // Check if username already exists
            var usernameExists = Repository.GetUserByUsername(model.Username);
            if (usernameExists != null)
            {
                ModelState.AddModelError("Username", "Username already exists!");
            }

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // After validation, create the new user object
            var user = new UserModel()
            {
                Username = model.Username,
                Email = model.Email,
                RegisterDate = DateTime.UtcNow
            };

            // Generate the password hash
            user.PasswordHash = CryptographyHelper.GetUserHashString(user, model.Password);

            // register the user
            Repository.RegisterUser(user);

            // If successful, redirect to login
            return RedirectToAction("Login");
        }

        public ActionResult Login()
        {
            var model = new LoginModel();

            var remember = Request.Cookies["RememberMe"];

            if (remember != null)
            {
                model.RememberMe = true;
                model.Username = remember.Value;
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Login(LoginModel model)
        {
            if(!ModelState.IsValid)
            {
                return View(model);
            }

            var userRepository = new UserRepository();

            // Get user by username
            var user = userRepository.GetUserByUsername(model.Username);

            if(user == null)
            {
                ModelState.AddModelError("Username", "No user with this username exists!");
                return View(model);
            }

            // Actual login
            var passwordHash = CryptographyHelper.GetUserHashString(user, model.Password);

            if(passwordHash != user.PasswordHash)
            {
                ModelState.AddModelError("Password", "Login was unsuccessful!");
                return View(model);
            }

            Session["CurrentUser"] = user;

            // Remember user
            if (model.RememberMe)
            {
                // TODO:: Implement this for REAL
                var cookie = new HttpCookie("RememberMe", model.Username);
                cookie.Expires = DateTime.Now.AddDays(30);

                Response.Cookies.Add(cookie);
            }

            return RedirectToAction("Index");
        }

        public void HeartBeat(bool? real)
        {
            if(CurrentUser == null) { return; }

            CurrentUser.LastHeartBeat = DateTime.UtcNow;

            if (real.HasValue && real.Value)
            {
                CurrentUser.LastActivity = DateTime.UtcNow;
            }

            Repository.UpdateUser(CurrentUser);
        }

        public ActionResult GetUserStatus(string userName)
        {
            var user = Repository.GetUserByUsername(userName);

            if(user == null) { return null; }

            return Json(new { status = user.Status, userName = user.Username });
        }

        public ActionResult Logout()
        {
            Session.Remove("CurrentUser");

            return RedirectToAction("Index", "Home");
        }
    }
}
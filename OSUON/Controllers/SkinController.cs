﻿using OSUON.Models;
using OSUON.Repositories;
using OSUON.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OSUON.Controllers
{
    public class SkinController : BaseController
    {
        public SkinRepository Repository;

        public SkinController()
        {
            Repository = new SkinRepository();
        }

        public ActionResult Index()
        {
            // No Beatmap selected error page 
            return View();
        }

        // GET: Skin
        [Route("Skin/Index/{id:int}")]
        public ActionResult Index(int id)
        {
            var model = new SkinViewModel();

            model.Skin = Repository.GetSkinById(id);

            return View(model);
        }

        public ActionResult Edit(int id)
        {
            // TODO: Get skin from repository;
            var model = new SkinModel();

            if(model == null)
            {
                // Beatmap does not exist
                return View();
            }

            if (CurrentUser.Id != model.OwnerId)
            {
                return RedirectToAction("Index", id);
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(SkinModel model)
        {
            if(CurrentUser.Id != model.OwnerId)
            {
                return View();
            }

            // Edit Skin details in DB

            // If all went well, redirect to overview
            return RedirectToAction("Index", model.Id);
        }
    }
}
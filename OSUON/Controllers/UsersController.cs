﻿using OSUON.Models;
using OSUON.Repositories;
using OSUON.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OSUON.Controllers
{
    public class UsersController : BaseController
    {
        public UserRepository Repository { get; set; }
        // GET: Skin

        public UsersController()
        {
            Repository = new UserRepository();
        }

        public ActionResult Index()
        {
            var model = new UsersOverviewViewModel();

            model.Users = new List<UserModel>();

            model.ActiveUsers = Repository.GetAllActiveUsers();

            model.Users = Repository.GetAllUsers();

            return View(model);
        }

        public ActionResult GetUsersStatus()
        {
            List<dynamic> userStatusses = new List<dynamic>();

            var users = Repository.GetAllActiveUsers();

            users.ForEach(user => userStatusses.Add(new { status = user.Status, userName = user.Username }));

            return Json(userStatusses);
        }
    }
}
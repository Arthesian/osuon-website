﻿using OSUON.Models;
using OSUON.Repositories;
using OSUON.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OSUON.Controllers
{
    public class SkinsController : BaseController
    {
        public SkinRepository Repository;

        public SkinsController()
        {
            Repository = new SkinRepository();
        }

        // GET: Skin
        public ActionResult Index()
        {
            var model = new SkinsOverviewViewModel();

            model.Skins = new List<SkinModel>();

            for(var x = 0; x < 8; x++)
            {
                model.Skins.Add(Repository.GetDummySkin());
            }

            return View(model);
        }
    }
}
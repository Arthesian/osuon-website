﻿using OSUON.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OSUON.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            var model = new BaseViewModel();

            return View(model);
        }
    }
}
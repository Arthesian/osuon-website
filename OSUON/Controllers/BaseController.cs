﻿using OSUON.Managers;
using OSUON.Models;
using OSUON.Repositories;
using OSUON.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace OSUON.Controllers
{
    public class BaseController : Controller
    {
        public UserModel CurrentUser { get { return (UserModel)Session["CurrentUser"]; } }

        public static BaseViewModel BaseModel { get; set; }

        public BaseController()
        {
            BaseModel = new BaseViewModel();
        }
    }
}
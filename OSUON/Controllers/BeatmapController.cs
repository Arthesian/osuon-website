﻿using OSUON.Repositories;
using OSUON.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OSUON.Controllers
{
    public class BeatmapController : BaseController
    {
        // GET: Beatmap
        public ActionResult Index()
        {
            var model = new BeatmapViewModel();

            model.Beatmap = new BeatmapRepository().GetDummyBeatmap();

            return View(model);
        }
    }
}
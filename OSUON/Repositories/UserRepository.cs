﻿using MongoDB.Bson;
using MongoDB.Driver;
using OSUON.Models;
using OSUON.ViewModels;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace OSUON.Repositories
{
    public class UserRepository : BaseRepository
    {
        private IMongoCollection<UserModel> UserCollection;
        
        public UserRepository()
        {
            this.UserCollection = _database.GetCollection<UserModel>("users");
        }

        public List<UserModel> GetAllUsers()
        {
            var result = UserCollection.AsQueryable().ToList();

            return result;
        }

        public List<UserModel> GetAllActiveUsers()
        {
            var result = UserCollection.AsQueryable().Where(x => x.LastHeartBeat > DateTime.UtcNow.AddSeconds(-10)).ToList();

            return result;
        }

        public UserModel GetUserByUsername(string username)
        {
            var result = UserCollection.AsQueryable().Where(x => x.Username.ToLower() == username.ToLower()).FirstOrDefault();

            return result;
        }

        public UserModel GetUserByLogin(string username, string passwordHash)
        {
            var result = UserCollection.Find(x => username.Equals(x.Username, StringComparison.InvariantCultureIgnoreCase) && x.PasswordHash == passwordHash).FirstOrDefault();

            return result;
        }

        public UserModel GetUserById(string id)
        {
            var result = UserCollection.Find(x => x.Id == id).FirstOrDefault();

            return result;
        }

        public void UpdateUser(UserModel user)
        {
            UserCollection.ReplaceOne(x => x.Id == user.Id, user);
        }

        public void RegisterUser(UserModel user)
        {
            UserCollection.InsertOne(user);
        }

        public UserModel GetDummyUser()
        {
            var user = new UserModel()
            {
                About = "The original account",
                BirthDay = new DateTime(1991, 12, 18),
                Culture = new CultureInfo("nl-NL"),
                Username = "Arthesian",
                ProfileImageUrl = "https://www.arthesian.pro/static/img/arthesian.png",
                Id = "",
                RegisterDate = new DateTime(2000, 1, 1)
            };

            return user;
        }

        public UserModel ProfileUpdate(UserModel original, UserModel update)
        {
            var user = original;

            user.About = update.About;
            user.BirthDay = update.BirthDay;
            user.ProfileImageUrl = update.ProfileImageUrl;

            return user;
        }

        public UserViewModel GetViewModel(UserModel user)
        {
            var model = new UserViewModel();
            model.User = user;

            model.AboutList = new Dictionary<string, string>();
            model.AboutList.Add("Country", user.Country);
            model.AboutList.Add("City", user.City);
            model.AboutList.Add("Birth Date", user.BirthDay.ToShortDateString());

            string hobbies = string.Join(",", (user.Hobbies ?? new List<string>()));
            model.AboutList.Add("Hobbies", hobbies);
            model.AboutList.Add("Occupation", user.Occupation);
            model.AboutList.Add("Website", !string.IsNullOrEmpty(user.WebsiteUrl) ? "<a href='" + user.WebsiteUrl + "'>" + user.WebsiteUrl + "</a>" : "");
            model.AboutList.Add("Last Activity", user.LastActivity.ToLongTimeString());
            model.AboutList.Add("Member since", user.RegisterDate.ToLongTimeString());

            // TODO: Get Friends 
            model.Friends = GetAllUsers();

            // TODO: Get Awards
            model.Awards = new Dictionary<string, string>();
            model.Awards.Add("SS", "99");
            model.Awards.Add("S", "387");
            model.Awards.Add("A", "1845");

            // TODO: Get statistics
            model.Statistics = new Dictionary<string, string>();
            model.Statistics.Add("Number of plays", (17234).ToString("N0"));
            model.Statistics.Add("Total score", (14567342345).ToString("N0"));
            model.Statistics.Add("Overal hitpercentage", (12.45).ToString("N") + "%");

            return model;
        }
    }
}
﻿using OSUON.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OSUON.Repositories
{
    public class BeatmapRepository : BaseRepository
    {
        public BeatmapModel GetBeatmapById(int id)
        {
            if(id == -1)
            {
                return GetDummyBeatmap();
            }
            else
            {
                // Get Beatmap from DB/Folder idk
            }

            return null;
        }

        public BeatmapModel GetDummyBeatmap()
        {
            var beatmap = new BeatmapModel()
            {
                Difficulties = new List<BeatmapDifficultyModel>(),
                Id = "",
                Artist = "Artist Name",
                Source = "Source of audio",
                Description = "Short version, TV Opening",
                OwnerId = ""
            };

            for (var x = 0; x < 3; x++)
            {
                beatmap.Difficulties.Add(new BeatmapDifficultyModel()
                {
                    Data = "",
                    Name = "Expert" + x,
                    Stars = x + 2.5F
                });
            }

            return beatmap;
        }
    }
}
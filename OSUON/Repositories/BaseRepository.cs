﻿using MongoDB.Driver;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OSUON.Repositories
{
    public class BaseRepository
    {
        protected IMongoClient _client;
        protected IMongoDatabase _database;

        public BaseRepository()
        {
            _client = new MongoClient("mongodb://arthesian:Iz15jb2mgcuUT99M@osuon-01-shard-00-00-bp0m4.mongodb.net:27017,osuon-01-shard-00-01-bp0m4.mongodb.net:27017,osuon-01-shard-00-02-bp0m4.mongodb.net:27017/test?ssl=true&replicaSet=OSUON-01-shard-0&authSource=admin");
            _database = _client.GetDatabase("OSUON-01");
        }
    }
}
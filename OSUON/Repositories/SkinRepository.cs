﻿using OSUON.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OSUON.Repositories
{
    public class SkinRepository : BaseRepository
    {
        public SkinModel GetSkinById(int id)
        {
            if(id == -1)
            {
                return GetDummySkin();
            }
            else
            {
                // Get skin from DB by ID
            }

            return null;
        }

        public SkinModel GetDummySkin()
        {
            var skin = new SkinModel()
            {
                Id = "",
                Author = "Arthesian",
                Description = "Arthesian's pro skin",
                ImageUrl = "",
                NumberOfRatings = 999,
                OwnerId = "",
                Rating = 9,
                Title = "Arthesian's Pro",
                Version = 1
            };

            return skin;
        }
    }
}
﻿using OSUON.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace OSUON
{
    public class CryptographyHelper
    {
        private static string _staticSalt = "a^Ro8CFHqn@yf$pVX1^A";

        public static byte[] GetHash(string inputString, string salt)
        {
            HashAlgorithm algorithm = SHA256.Create();

            return algorithm.ComputeHash(Encoding.UTF8.GetBytes(inputString));
        }

        public static string GetHashString(string inputString, string salt)
        {
            StringBuilder sb = new StringBuilder();
            foreach (byte b in GetHash(inputString, salt))
                sb.Append(b.ToString("X2"));

            return sb.ToString();
        }

        public static string GetUserHashString(UserModel user, string password)
        {
            var generatedSalt = GetHashString(user.RegisterDate.ToLongTimeString() + user.Username, _staticSalt);

            return GetHashString(password, generatedSalt);
        }
    }
}
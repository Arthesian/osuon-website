﻿using System.Web;
using System.Web.Optimization;

namespace OSUON
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/js/libs").Include(
                        "~/static/js/jquery-3.2.1.min.js",
                        "~/static/js/createjs.2015.11.26.min.js",
                        "~/static/js/tether.min.js",
                        "~/static/js/bootstrap.min.js",
                        "~/static/js/bootstrap-slider.min.js",
                        "~/Scripts/respond.min.js",
                        "~/Scripts/jquery.validate*",
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/js/main").Include(
                "~/static/js/modules/module-user-heartbeat.js",
                "~/static/js/modules/module-game-ticker.js",
                "~/static/js/modules/module-game-controls.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new StyleBundle("~/css").Include(
                      "~/static/style/bootstrap.min.css",
                      "~/static/style/bootstrap-slider.min.css",
                      "~/static/style/flags.min.css",
                      "~/static/style/font-awesome.min.css",
                      "~/static/style/tether.min.css",
                      "~/static/style/main.css"));

            
        }
    }
}
